# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'hercule/version'

Gem::Specification.new do |gem|
  gem.name          = "hercule"
  gem.version       = Hercule::VERSION
  gem.authors       = ["Gustavo Giráldez"]
  gem.email         = ["ggiraldez@manas.com.ar"]
  gem.description   = %q{}
  gem.summary       = %q{}
  gem.homepage      = "https://bitbucket.org/instedd/hercule"

  gem.add_dependency 'elasticsearch'

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end

